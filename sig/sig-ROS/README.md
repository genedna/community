# openEuler ROS SIG

- 在openEuler社区中添加对ROS和ROS2的支持
- 根据openEuler迭代版本，持续完成ROS和ROS2中各个组件向openEuler的移植，并提供ROS和ROS2在openEuler上的使用文档
- 及时响应用户反馈，解决相关问题


# 组织会议

- 公开的会议时间：北京时间，双周周四 晚上，7点-8点


# 成员

### Maintainer列表

- 吴伟[@wuwei_plct](https://gitee.com/wuwei_plct)
- 安传旭[@anchuanxu](https://gitee.com/anchuanxu)
- 王晓云[@xiao_yun_wang](https://gitee.com/xiao_yun_wang)

### Committer列表

- 吴伟[@wuwei_plct](https://gitee.com/wuwei_plct)
- 安传旭[@anchuanxu](https://gitee.com/anchuanxu)
- 王晓云[@xiao_yun_wang](https://gitee.com/xiao_yun_wang)
- 韩昊旻[@davidhan008](https://gitee.com/davidhan008)
- 席江玲[@simonaxi](https://gitee.com/simonaxi)
- 陈家友[@chen_jiayou](https://gitee.com/chen_jiayou)

# 联系方式

- [邮件列表](dev@openeuler.org)
- [IRC频道](#openeuler-ros)
- [IRC公开会议](#openeuler-meeting)


# 项目清单

项目名称：ROS

repository地址：

- https://gitee.com/openeuler/ros
- https://gitee.com/src-openeuler/catkin
- https://gitee.com/src-openeuler/class_loader
- https://gitee.com/src-openeuler/cmake_modules
- https://gitee.com/src-openeuler/gencpp
- https://gitee.com/src-openeuler/geneus
- https://gitee.com/src-openeuler/genlisp
- https://gitee.com/src-openeuler/genpy
- https://gitee.com/src-openeuler/gennodejs
- https://gitee.com/src-openeuler/genmsg
- https://gitee.com/src-openeuler/std_msgs
- https://gitee.com/src-openeuler/message_generation
- https://gitee.com/src-openeuler/ros_environment
- https://gitee.com/src-openeuler/message_runtime
- https://gitee.com/src-openeuler/roscpp_core
- https://gitee.com/src-openeuler/ros
- https://gitee.com/src-openeuler/rosconsole
- https://gitee.com/src-openeuler/roslisp
- https://gitee.com/src-openeuler/rospack
- https://gitee.com/src-openeuler/ros_comm_msgs
- https://gitee.com/src-openeuler/pluginlib
- https://gitee.com/src-openeuler/ros_comm
- https://gitee.com/src-openeuler/actionlib 
- https://gitee.com/src-openeuler/angles
- https://gitee.com/src-openeuler/bond_core
- https://gitee.com/src-openeuler/dynamic_reconfigure
- https://gitee.com/src-openeuler/geometry
- https://gitee.com/src-openeuler/geometry2
- https://gitee.com/src-openeuler/nodelet_core
- https://gitee.com/src-openeuler/common_msgs
- https://gitee.com/src-openeuler/openslam_gmapping
- https://gitee.com/src-openeuler/slam_gmapping 
- https://gitee.com/src-openeuler/urdf
- https://gitee.com/src-openeuler/rosconsole_bridge
- https://gitee.com/src-openeuler/rviz
- https://gitee.com/src-openeuler/pcl_conversion
- https://gitee.com/src-openeuler/orocos_kdl
- https://gitee.com/src-openeuler/resource_retriever 
- https://gitee.com/src-openeuler/cartographer
- https://gitee.com/src-openeuler/cartographer_ros
- https://gitee.com/src-openeuler/image_transport
- https://gitee.com/src-openeuler/interactive_markers
- https://gitee.com/src-openeuler/laser_geometry
- https://gitee.com/src-openeuler/navigation_msgs
- https://gitee.com/src-openeuler/pcl_msgs
- https://gitee.com/src-openeuler/python_qt_binding
- https://gitee.com/src-openeuler/robot_state_publisher 
- https://gitee.com/src-openeuler/perception_pcl
- https://gitee.com/src-openeuler/image_common
- https://gitee.com/src-openeuler/kdl_parser
- https://gitee.com/src-openeuler/diagnostics
- https://gitee.com/src-openeuler/navigation
- https://gitee.com/src-openeuler/console_bridge
- https://gitee.com/src-openeuler/poco
- https://gitee.com/src-openeuler/stage
- https://gitee.com/src-openeuler/stage_ros
- https://gitee.com/src-openeuler/urdf_sim_tutorial
- https://gitee.com/src-openeuler/urdf_tutorial
- https://gitee.com/src-openeuler/urdfdom_py
- https://gitee.com/src-openeuler/xacro
- https://gitee.com/src-openeuler/webkit_dependency
- https://gitee.com/src-openeuler/rqt_action
- https://gitee.com/src-openeuler/gl_dependency
- https://gitee.com/src-openeuler/laser_assembler
- https://gitee.com/src-openeuler/laser_filters
- https://gitee.com/src-openeuler/laser_pipeline
- https://gitee.com/src-openeuler/media_export
- https://gitee.com/src-openeuler/qwt_dependency
- https://gitee.com/src-openeuler/realtime_tools
- https://gitee.com/src-openeuler/roslint
- https://gitee.com/src-openeuler/control_toolbox
- https://gitee.com/src-openeuler/filter
- https://gitee.com/src-openeuler/ros_tutorials
- https://gitee.com/src-openeuler/ros_control
- https://gitee.com/src-openeuler/rqt_bag
- https://gitee.com/src-openeuler/rqt_pose_view
- https://gitee.com/src-openeuler/executive_smach
- https://gitee.com/src-openeuler/image_pipeline
- https://gitee.com/src-openeuler/common_tutorials
- https://gitee.com/src-openeuler/geometry_tutorials
- https://gitee.com/src-openeuler/qt-gui
- https://gitee.com/src-openeuler/rqt
- https://gitee.com/src-openeuler/image_transport_plugins
- https://gitee.com/src-openeuler/control_msgs
- https://gitee.com/src-openeuler/joint_state_publisher
- https://gitee.com/src-openeuler/orocos_kinematics_dynamics
- https://gitee.com/src-openeuler/ros_controllers
- https://gitee.com/src-openeuler/rqt_rviz
- https://gitee.com/src-openeuler/rqt_runtime_monitor
- https://gitee.com/src-openeuler/rqt_service_caller
- https://gitee.com/src-openeuler/rqt_shell
- https://gitee.com/src-openeuler/rqt_srv
- https://gitee.com/src-openeuler/rqt_tf_tree
- https://gitee.com/src-openeuler/rqt_top
- https://gitee.com/src-openeuler/rqt_topic
- https://gitee.com/src-openeuler/rqt_web
- https://gitee.com/src-openeuler/rosbag_migration_rule
- https://gitee.com/src-openeuler/rqt_robot_plugins
- https://gitee.com/src-openeuler/rqt_robot_dashboard
- https://gitee.com/src-openeuler/rqt_reconfigure
- https://gitee.com/src-openeuler/rqt_py_console
- https://gitee.com/src-openeuler/rqt_plot
- https://gitee.com/src-openeuler/rqt_graph
- https://gitee.com/src-openeuler/rqt_dep
- https://gitee.com/src-openeuler/rqt_image_view
- https://gitee.com/src-openeuler/rqt_common_plugins
- https://gitee.com/src-openeuler/rqt_console
- https://gitee.com/src-openeuler/rqt_launch
- https://gitee.com/src-openeuler/rqt_logger_level
- https://gitee.com/src-openeuler/rqt_moveit
- https://gitee.com/src-openeuler/gazebo_ros_pkgs
- https://gitee.com/src-openeuler/rqt_msg
- https://gitee.com/src-openeuler/rqt_nav_view
- https://gitee.com/src-openeuler/rqt_publisher
- https://gitee.com/src-openeuler/rqt_robot_monitor
- https://gitee.com/src-openeuler/rqt_robot_steering
- https://gitee.com/src-openeuler/vision_opencv
- https://gitee.com/src-openeuler/visualization_tutorials

项目名称：ROS2

repository地址：

- https://gitee.com/src-openeuler/ament_cmake
- https://gitee.com/src-openeuler/ament_index
- https://gitee.com/src-openeuler/ament_lint
- https://gitee.com/src-openeuler/ament_package
- https://gitee.com/src-openeuler/googletest
- https://gitee.com/src-openeuler/Fast-CDR
- https://gitee.com/src-openeuler/python_cmake_module
- https://gitee.com/src-openeuler/console_bridge_vendor
- https://gitee.com/src-openeuler/ament_cmake_ros
- https://gitee.com/src-openeuler/tinyxml_vendor
- https://gitee.com/src-openeuler/mimick_vendor
- https://gitee.com/src-openeuler/uncrustify_vendor
- https://gitee.com/src-openeuler/cyclonedds
- https://gitee.com/src-openeuler/osrf_pycommon
- https://gitee.com/src-openeuler/osrf_testing_tools_cpp
- https://gitee.com/src-openeuler/libyaml_vendor
- https://gitee.com/src-openeuler/test_interface_files
- https://gitee.com/src-openeuler/google_benchmark_vendor
- https://gitee.com/src-openeuler/Fast-DDS
- https://gitee.com/src-openeuler/performance_test_fixture
- https://gitee.com/src-openeuler/foonathan_memory_vendor
- https://gitee.com/src-openeuler/tinyxml2_vendor
- https://gitee.com/src-openeuler/rosidl
- https://gitee.com/src-openeuler/tlsf
- https://gitee.com/src-openeuler/spdlog_vendor
- https://gitee.com/src-openeuler/sros2
- https://gitee.com/src-openeuler/rpyutils
- https://gitee.com/src-openeuler/rcpputils
- https://gitee.com/src-openeuler/launch
- https://gitee.com/src-openeuler/launch_ros
- https://gitee.com/src-openeuler/rosidl_dds
- https://gitee.com/src-openeuler/rosidl_defaults
- https://gitee.com/src-openeuler/rcl_interfaces
- https://gitee.com/src-openeuler/unique_identifier_msgs
- https://gitee.com/src-openeuler/rmw
- https://gitee.com/src-openeuler/rmw_cyclonedds
- https://gitee.com/src-openeuler/rmw_dds_common
- https://gitee.com/src-openeuler/eigen3_cmake_module
- https://gitee.com/src-openeuler/rosidl_python
- https://gitee.com/src-openeuler/rosidl_typesupport
- https://gitee.com/src-openeuler/yaml_cpp_vendor
- https://gitee.com/src-openeuler/rcutilsl